<?php



namespace shortcode\src;

class StatsHandler
{
    private $db;
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function MOCount()
    {
        $sql = 'SELECT  COUNT(ID) AS count
        				FROM `MO`
        				WHERE `created_at` > NOW() - INTERVAL 15 MINUTE';

         $this->db->query($sql);
        $result = $this->db->single();

        return $result;

    }

    public function timeSpan()
    {
        $sql = 'SELECT min(`created_at`) AS min_date, max(`created_at`) max_date FROM
        				(SELECT `created_at` FROM `MO` ORDER BY `ID` DESC LIMIT 10000) AS tsl ';

        $this->db->query($sql);
        $result = $this->db->resultset();

        return $result;
    }
}
