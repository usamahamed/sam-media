<?php



namespace shortcode\src;

class Validator
{
    public static function validate($queueContents, $validKeys)
    {
        if (!is_array($queueContents)) {
            
            throw new \InvalidArgumentException();
}

        if (count($queueContents) != count($validKeys)) {
            return false;
        }

        foreach ($queueContents as $key => $value) {
             if($key!="cookiclass"){
                 
            if (empty($value)) {
               
                return false;
            }
            
            }
        }

        return true;
    }
}
