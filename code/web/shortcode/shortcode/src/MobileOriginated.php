<?php


namespace shortcode\src;

use Resque;

class MobileOriginated
{
    public static $countinvalidjobs=0;
    public $db;
    public $resque;
    private function getAuthToken($input)
    {
        $arg = json_encode($input);

        return `./registermo $arg`;
    }

    public function setDatabase(Database $db)
    {
        $this->db = $db;
    }

    public function setResque($resque)
    {
        $this->resque = $resque;
    }

    public function insert($stackValues)
    {
        if (!is_array($stackValues) || count($stackValues) == 0) {
            return false;
        }

        $this->db->query('INSERT INTO
                `MO` (msisdn, operatorid, shortcodeid,text, auth_token, created_at)
                 VALUES (:msisdn,:operatorid,:shortcodeid,:text,:auth_token, NOW())'
            );
        $this->db->beginTransaction();
        foreach ($stackValues as $item) {
            echo 'generating new token !'.PHP_EOL;
            $auth_token = $this->getAuthToken($stackValues);
            $this->db->bind(':msisdn', $item['msisdn']);
            $this->db->bind(':operatorid', $item['operatorid']);
            $this->db->bind(':shortcodeid', $item['shortcodeid']);
            $this->db->bind(':text', $item['text']);
            $this->db->bind(':auth_token', $auth_token);
            $this->db->execute();
        }
        $this->db->endTransaction();

        echo 'A new job batch inserted into database '.PHP_EOL;
        echo 'the number of MO\'s received but not yet processed:- '.self::$countinvalidjobs.PHP_EOL;
    }

    public function createJobStack($input, $queue = null)
    {
        if (!is_array($input)) {
            throw new \InvalidArgumentException();
        }


        $stack = $queue.'-jobstack';
        $size = $this->resque->size($stack);
        if ($size < Config::$minQueueSize) {

            
            if (!Validator::validate($input, Config::$validKeys)) {
                echo 'invalide content'.PHP_EOL;
                self::$countinvalidjobs++;
                return false;
            }
            
            $this->resque->push($stack, $input);
            echo 'new item added to '.$stack.PHP_EOL;
            echo 'current stack size is : '.$size.PHP_EOL;
        } else {
            $stackValues = [];
            for ($i = 1; $i <= Config::$minQueueSize; ++$i) {
                array_push($stackValues, $this->resque->pop($stack));
            }

            echo 'calling insert method !'.PHP_EOL;
            $this->insert($stackValues);
        }

        return true;
    }

    public function setUp()
    {
        $this->setDatabase((new Database()));
        $this->setResque((new Resque()));
    }
    public function perform()
    {
        $this->createJobStack($this->args, $this->queue);
    }
}