<?php


namespace shortcode\src;

class Config
{
    public static $minQueueSize = 5;

    public static $dbDetails = [
            'db_name' => 'samtt',
            'db_user' => 'root',
            'db_pass' => 'root',
            'db_host' => 'localhost',
        ];

    public static $validKeys = [
            'msisdn',
            'operatorid',
            'shortcodeid',
             'text',
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
        
        ];
}