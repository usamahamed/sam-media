<?php



namespace shortcode\src\Tests;

use shortcode\src\Validator;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    private $validator;
    public function setUp()
    {
        $this->validator = new Validator();
    }

    public function FakeArgs()
    {
        return [
                ['string'],
                [1],
                [2.5],
                [-3],
            ];
    }

        /**
         * @dataProvider FakeArgs
         * @expectedException InvalidArgumentException
         */
        public function testNonArrayArgumentPassedToValidate($input)
        {
            $this->validator->validate($input, $input);
        }

    public function testCountArrayValuesIsEquals()
    {
        $arr1 = [
                 'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            ];

        $arr2 = [
                 'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            ];
        $this->assertTrue($this->validator->validate($arr1, $arr2));
    }

    public function testCountArrayValuesIsNotEquals()
    {
        $arr1 = [
                 'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            ];

        $arr2 = [
                'msisdn',
             'text',
            ];

        $this->assertFalse($this->validator->validate($arr1, $arr2));
    }

    public function testCountArrayValuesIsEmpty()
    {
        $arr1 = [
                'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            ];

        $arr2 = [
                'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            ];

        $this->assertFalse($this->validator->validate($arr1, $arr2));
    }

    public function testCountArrayValuesIsNotEmpty()
    {
        $arr1 = [
                'msisdn' => '1' ,
                'operatorid' => '2',
                'shortcodeid' => '3',
                 'text' => '4',
            
            ];

        $arr2 = [
                  'msisdn',
            'operatorid',
            'shortcodeid',
             'text',
            ];

        $this->assertTrue($this->validator->validate($arr1, $arr2));
    }
}
