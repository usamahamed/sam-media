<?php



namespace shortcode\src\Tests;

use shortcode\src\MobileOriginated;

class MobileOriginatedTest extends \PHPUnit_Framework_TestCase
{
    protected $database;
    protected $resque;
    protected $handler;

    public function setUp()
    {
        $this->database = $this->getMock('shortcode\src\Database', array('query', 'beginTransaction', 'bind', 'execute', 'endTransaction'));
        $this->handler = new LeadCollector();
        $this->handler->setDatabase($this->database);

        $this->resque = $this->getMock('ResqueFake', array('size', 'push', 'pop'));
        $this->handler->setResque($this->resque);
    }

    public function TestInsertionValidData()
    {
        $input = array();

        $this->assertFalse($this->handler->save($input));
    }
  public function testNewJobStackWithCorrentData()
    {
        $this->resque->expects($this->once())->method('size')->with('sample-jobstack')->will($this->returnValue('1'));
        $correct_data = array(
                 'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
                );

        $this->assertTrue($this->handler->createJobStack($correct_data, 'sample'));
    }
    public function TestIfInsertionWithCorrectData()
    {
        $input = array(
                array(
                 'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
                    ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
                ),
            );

        $this->database
                ->expects($this->exactly(4))->method('bind')->withAnyParameters();

        $this->database
                ->expects($this->at(2))
                ->method('bind')
                ->with(':msisdn', 'msisdn')
            ;

        $this->database
                ->expects($this->once())
                ->method('query')
            ;
        $this->assertNull($this->handler->insert($input));
    }

    public function testNewJobStackWithIncorrentData()
    {
        $this->resque->expects($this->once())->method('size')->with('sample-jobstack')->will($this->returnValue('1'));
        $incorrect_data = array();

        $this->assertFalse($this->handler->createJobStack($incorrect_data, 'sample'));
    }
    
        public function TestNotStackOverflow()
    {
        $mockHandler = $this->getMock('\shortcode\src\MobileOriginated', array('save'));
        $mockHandler->expects($this->once())->method('save')->will($this->returnValue(true));

        $mock_resque = $this->getMock('SomeClass', array('size', 'push', 'pop'));
        $mockHandler->setResque($mock_resque);

        $mock_resque->expects($this->once())->method('size')->with('sample-jobstack')->will($this->returnValue(\shortcode\src\Config::$minQueueSize));
        $correct_data = array(
                'msisdn',
            'operatorid',
            'shortcodeid',
             'text'
            ,
             '_kustomer_tracking',
             'cookiclass',
             'SQLiteManager_currentLangue'
            );

        $mock_resque
                ->expects($this->exactly(\shortcode\src\Config::$minQueueSize))->method('pop')->withAnyParameters()->will($this->returnValue(true));

        $this->assertTrue($mockHandler->createJobStack($correct_data, 'sample'));
    }

  


}
