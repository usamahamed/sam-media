<?php



namespace shortcode\src\Tests;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    public function ConfigAttr()
    {
        return [
                ['minQueueSize'],
                ['dbDetails'],
                ['validKeys'],
            ];
    }

    /**
     * @dataProvider configStaticAttrs
     */
    public function testIfStaticAttrExist($attr)
    {
        $this->assertClassHasStaticAttribute($attr, '\shortcode\src\Config');
    }
}
