<?php

    require 'vendor/autoload.php';

    Resque::setBackend('localhost:6379');
    $jobId = Resque::enqueue('default', '\shortcode\src\MobileOriginated', $_REQUEST, true);
    $status = new Resque_Job_Status($jobId);
    echo "Queued job in 'default' Queue with jobId : {$jobId} ".PHP_EOL;
    if($status->get()===1){
    echo  "Job is still queued".PHP_EOL; // Outputs the status
    }
    elseif ($status->get()===2) {
        echo  "Job is currently running".PHP_EOL; // Outputs the status

}
  elseif ($status->get()===3) {
        echo  "Job has failed".PHP_EOL; // Outputs the status

}
  elseif ($status->get()===4) {
        echo  "Job is complete".PHP_EOL; // Outputs the status

}