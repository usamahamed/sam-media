<?php


    $start = microtime(true);
    require 'vendor/autoload.php';
    use shortcode\src\Database;
    use shortcode\src\StatsHandler;

    $stats = new StatsHandler((new Database()));
    $response = [
        'last_15_min_mo_count' => $stats->MOCount(),
        'time_span_last_10k' => $stats->timeSpan(),
    ];

    echo PHP_EOL.json_encode($response).PHP_EOL;
    echo PHP_EOL.'Total Execution Time: '.(microtime(true) - $start).PHP_EOL;
